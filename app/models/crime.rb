class Crime < ActiveRecord::Base
  has_attached_file :file

  def self.import(file)
    CSV.foreach(file_path, :headers => true) do |row|
     Crime.create!(row.to_hash)
    end
  end
end
